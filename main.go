package main

//go:generate protoc -I ../towerapi/v1 telecomtower.proto --go_out=plugins=grpc:../towerapi/v1

import (
	"fmt"
	"net"
	"os"
	"os/signal"
	"sync"

	"github.com/jessevdk/go-flags"

	ws2811 "github.com/rpi-ws281x/rpi-ws281x-go"
	log "github.com/sirupsen/logrus"
	renderer "gitlab.com/telecom-tower/grpc-renderer"
)

const (
	ledCount = 1024
	dmaNum   = 10
)

var version = "master"

func main() { // nolint: gocyclo
	var opts struct {
		Verbose    []bool `short:"v" long:"verbose" description:"Show verbose debug information"`
		Version    bool   `long:"version" description:"Show version information"`
		Brightness int    `short:"b" long:"brightness" default:"128" env:"TOWER-BRIGHTNESS" description:"LED Brightness"`
		Port       int    `short:"p" long:"port" default:"10000" env:"TOWER-PORT" description:"gRPC port"`
	}

	_, err := flags.Parse(&opts)
	if err != nil {
		panic(err)
	}

	if opts.Version {
		fmt.Printf("Telecom Tower Server // version : %v\n", version)
		os.Exit(0)
	}

	if len(opts.Verbose) > 1 {
		log.SetLevel(log.DebugLevel)
	} else if len(opts.Verbose) > 0 {
		log.SetLevel(log.InfoLevel)
	} else {
		log.SetLevel(log.WarnLevel)
	}

	// Create and run hub
	wsopt := ws2811.DefaultOptions
	wsopt.DmaNum = dmaNum
	wsopt.Channels[0].Brightness = opts.Brightness
	wsopt.Channels[0].LedCount = ledCount
	ws, err := ws2811.MakeWS2811(&wsopt)
	if err != nil {
		log.Fatal(err)
	}
	if err = ws.Init(); err != nil {
		log.Fatal(err)
	}

	wg := &sync.WaitGroup{}
	grpcLis, err := net.Listen("tcp", fmt.Sprintf(":%d", opts.Port))
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}
	renderer := renderer.NewRenderer(ws)
	wg.Add(1)
	go func() {
		err := renderer.Serve(grpcLis)
		if err != nil {
			log.Error(err)
		}
		wg.Done()
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	renderer.Shutdown()

	wg.Wait()
}
