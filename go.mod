module gitlab.com/telecom-tower/server

require (
	github.com/jessevdk/go-flags v1.4.0
	github.com/rpi-ws281x/rpi-ws281x-go v1.0.5
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/telecom-tower/grpc-renderer v1.4.0
	golang.org/x/image v0.0.0-20190910094157-69e4b8554b2a // indirect
	golang.org/x/net v0.0.0-20190909003024-a7b16738d86b // indirect
	golang.org/x/sys v0.0.0-20190911201528-7ad0cfa0b7b5 // indirect
	google.golang.org/genproto v0.0.0-20190911173649-1774047e7e51 // indirect
	google.golang.org/grpc v1.23.1 // indirect
)

go 1.13
