FROM balenalib/raspberrypi3-golang:latest-build AS builder

WORKDIR /tmp
RUN [ "cross-build-start" ]
RUN apt-get update -y && apt-get install -y scons
RUN git clone https://github.com/jgarff/rpi_ws281x.git && \
    cd rpi_ws281x && \
    scons
RUN cp /tmp/rpi_ws281x/*.a /usr/local/lib/ && \
    cp /tmp/rpi_ws281x/*.h /usr/local/include/
COPY go.* *.go .version /build/
WORKDIR /build
RUN GOARM=7 go build -o tower-server -v -ldflags "-X main.version=$(cat .version)" ./...
RUN [ "cross-build-end" ]

FROM balenalib/raspberrypi3:latest-run
COPY --from=builder /build/tower-server /usr/local/bin/
EXPOSE 10000
CMD /usr/local/bin/tower-server