docker run --rm -ti \
  -v "$(pwd)":/project supcik/rpi-ws281x-go-builder\
  /usr/bin/qemu-arm-static \
  /bin/sh -c "cd /project; GOARM=6 go build -o tower-server-armv6 -v ./...; GOARM=7 go build -o tower-server-armv7 -v ./...;"
