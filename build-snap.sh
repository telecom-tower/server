#!/bin/bash

rm -Rf snap-build
mkdir -p snap-build/usr/bin
cp tower-server-armv7 snap-build/usr/bin/tower-server
tar czvf snap-root.tar.gz -C snap-build .
tar tzvf snap-root.tar.gz

snapcraft

rm -Rf snap-build
rm -Rf snap-root.tar.gz